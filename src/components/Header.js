import React, { useState ,useEffect} from 'react';
import { FormControl, MenuItem, Select } from '@material-ui/core';

///v3/covid-19/countries


const Header = () => {
    //set countries state
    const [countries, setCountries] = useState([]);

    const [country, setCountry] = useState('worldlwide');
    const [countryInfo, setCountryInfo] = useState({});

    useEffect(() => {
        //API for getting countries data
        const getCountriesData = async () =>{
            await fetch("https://disease.sh/v3/covid-19/countries")
            .then(
                //get the response data
                (response) => response.json()
            ).then(
                (data) =>{
                    //loop thorugh the data
                    const countries = data.map(
                        country => ({
                                name:country.country, //countries names
                                value:country.countryInfo.iso2 //countries initials
                            }));

                            setCountries(countries);
                });
        }
    
        getCountriesData();
    }, []);

    const onCountryChange = async (event) =>{
        //get selected country value;
        const countryCode = event.target.value;
        //set selected country
        setCountry(countryCode);

        //https://disease.sh/v3/covid-19/countries/UK
        //get data by country after select a specific country
        const url = countryCode === 'worlwide' ? 'https://disease.sh/v3/covid-19/countries/UK' 
        : `https://disease.sh/v3/covid-19/countries/${countryCode}`;


        await fetch(url)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            setCountryInfo(data);

            return data;
        })

    }

    return (
        <div className="form__wrapper">
            <FormControl className="app__dropdown">
                <Select variant="outlined" onChange={onCountryChange} value={country}>

                {/* loop through countries list*/}
                <MenuItem value="worldlwide">Wordlwide</MenuItem>
                {
                    countries.map((country) => (
                       
                        <MenuItem value={country.value}>{country.name}</MenuItem>

                    ))
                }
             
                </Select>
            </FormControl>
        </div>
    )
}

export default Header;
