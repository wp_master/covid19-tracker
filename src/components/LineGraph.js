
import { Line } from "react-chartjs-2";
import React, { useState , useEffect} from 'react';
import numeral from "numeral";

//chart options configuration
const options = {
    legend:{
        display:false,
    },
    elements: {
        point: {
            radius:0,
        },
    },
    maintainAspectRatio: false,
    tooltips: {
        mode:"index",
        intersect: false,
        callbacks: {
            label: function (tooltipItem, data){
                return numeral(tooltipItem.value).format("+0,0");
            }
        }
    },
    scales: {
        xAxes: [
            {
                type:"time",
                time: {
                    format: "MM/DD/YY",
                    tooltipFormat: "ll",
                },
            },
        ],
        yAxes: [
            {
                gridLines: {
                    display: false,
                },
                ticks: {
                    //include dollar sign in the ticks
                    callback: function (value, index, values){
                        return numeral(value).format("0a");
                    },
                },
            },
        ]
    }
}


const buildChartData = (data,casesType ="cases") => {
    const chartData = [];

    let lastDataPoint;
    //loop through cases data
    for(let date in data.cases){

        //check last data point
        if(lastDataPoint){
            //create new data point and calculate new cases,
            
            const newDataPoint = {
                x:date,
                y:data[casesType][date] - lastDataPoint
            }
            //push new data for line chart object
            chartData.push(newDataPoint);
        }

        lastDataPoint = data[casesType][date];
    };

    return chartData;
   
};


const LineGraph = ({ casesType = 'cases' ,...props}) => {
    const [data, setData] = useState({});

    //curl -X GET "https://disease.sh/v3/covid-19/historical?lastdays=120" -H "accept: application/json"
    useEffect(() => {
            const fetchData = async () =>{
                await  fetch('https://disease.sh/v3/covid-19/historical/all?lastdays=120')
                .then(response => response.json())
                .then(data => {
                   //console.log(data);
       
                   const chartData = buildChartData(data, casesType);
                   setData(chartData);
    
                });
            }
        
            fetchData();
    }, [casesType]);


    return (
        <div className={props.className}>
        { 
            data?.length > 0 && (
            <Line 
            options={options}
            data ={{
                datasets:[
                    {
                        backgroundColor: "rgba(204, 16 , 52, 0.5)",
                        borderColor: "#CC1034",
                        data:data
                    },
                ],
            }}
            />

            )}
        </div>
    )
}

export default LineGraph;
