import React from 'react';
import '../css/Table.css';
import numeral from "numeral";

const Table = ({countries}) => {
    return (
        <div className="table">
            {/*Destructor the countries data inside the map befor using it*/ }
            {
                countries.map(({country, cases}) => (
                    
                        <tr>
                            <td>{country}</td>
                            <td><strong>{numeral(cases).format("0,0")}</strong></td>
                        </tr>
                   
                ))
            
            
            }
        </div>
    )
}

export default Table
