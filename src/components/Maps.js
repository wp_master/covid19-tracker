import React from 'react';
import { Map as LeafletMap, TileLayer } from "react-leaflet";
//import {Map} from "leaflet";
import "../css/Map.css";
import { showDataOnMap } from '../helpers/util';

 function Maps({countries, casesType ,center, zoom}) {
    
    
    return (
        <div className="map">
            <LeafletMap center={center} zoom={zoom}>
                <TileLayer 
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution = '&copy; <a href="http://osm.org/copyright">
                    openStreetMap</a>' 
                    

                />
                {/* Loop through countries and draq circles*/}
                {showDataOnMap(countries,casesType)}
            </LeafletMap>
            
        </div>
    );
}

export default Maps;