import "./App.css";
import { Card, CardContent, Typography } from "@material-ui/core";
import React, { useState ,useEffect} from 'react';
import { FormControl, MenuItem, Select } from '@material-ui/core';
import InfoBox from "./components/InfoBox";
import Maps from "./components/Maps";
import Table from "./components/Table";
import LineGraph from "./components/LineGraph";
import { sortData,preetyPrintStat } from "./helpers/util";
import "leaflet/dist/leaflet.css";
import logo from './assets/images/logo_192x192.png';

function App() {

  //set countries states
  const [countries, setCountries] = useState([]);
  const [country, setCountry] = useState("worldwide");
  const [countryInfo, setCountryInfo] = useState({});
  //set table data state
  const [tableData, setTableData] = useState([]);
  //set maps parameters
  const [mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -40.4796 });
  const [mapZoom, setMapZoom] = useState(2);
  const [mapCountries, setMapCountries] = useState([]);
  const [casesType, setCasesTypes] = useState("cases");
  

  //Default wordwide value for select country list
  useEffect( ()=>{
    //get worldwide data
    fetch("https://disease.sh/v3/covid-19/all")
    .then(response => response.json())
    .then( data => { setCountryInfo(data)});
  },[] );

  useEffect(() => {
    //API for getting countries data
    const getCountriesData = async () => {
      await fetch("https://disease.sh/v3/covid-19/countries")
        .then(
          //get the response data
          (response) => response.json()
        )
        .then((data) => {
          //loop thorugh the data
          const countries = data.map((country) => ({
            id:country.countryInfo.id,
            name: country.country, //countries names
            value: country.countryInfo.iso2, //countries initials
          }));
          //sorted data bedore setting values
          const sortedData = sortData(data);
          //set table new data
          setTableData(sortedData);
          //set countries table list
          setCountries(countries);
          setMapCountries(data);
        });
    };

    getCountriesData();
  }, []);
  //set country value on change function
  const onCountryChange = async (event) => {
    //get selected country value;
    const countryCode = event.target.value;
    //set selected country
    setCountry(countryCode);

    //get data by country after select a specific country
    const url =
      countryCode === "worldwide"
        ? "https://disease.sh/v3/covid-19/all"
        : `https://disease.sh/v3/covid-19/countries/${countryCode}`;

    await fetch(url)
      .then((response) => response.json())
      .then((data) => {
        //set selectd country
        setCountry(countryCode);
        //set data from the country response
        setCountryInfo(data);
        //set map location to the selected country  
        setMapCenter([data.countryInfo.lat, data.countryInfo.long]);
        setMapZoom(5);
    
        //setMapCenter([data.countryInfo.lat,data.countryInfo.long]);
        

        
        console.log('after select',mapCenter);
        console.log('after zoom',mapZoom);

      });
  };

  return (
    <div className="app">
      <div className="app__left">
        <div className="app__header">
        
          <h1>
          <img src={ logo }  className="app__logo"/>
          Covid-19 Tracker
          
          </h1>
          <div className="form__wrapper">
            <FormControl className="app__dropdown" >
              <Select
                variant="outlined"
                onChange={onCountryChange}
                value={country}
              >
                {/* loop through countries list*/}
                <MenuItem value="worldwide" key={country.id}>Wordlwide</MenuItem>
                {countries.map((country) => (
                  <MenuItem value={country.value} key={country.id}>{country.name}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
        </div>

        <div className="app_stats">
          <InfoBox 
          isRed
          active={casesType === "cases"}
          onClick={ (e) => setCasesTypes('cases') }
          title="Coronoa Cases" cases={preetyPrintStat(countryInfo.todayCases)} total={preetyPrintStat(countryInfo.cases)} />
          <InfoBox
          active={casesType === "recovered"} 
          onClick={ (e) => setCasesTypes('recovered') }
          title="Recovered" cases={preetyPrintStat(countryInfo.todayRecovered)} total={preetyPrintStat(countryInfo.recovered)} />
          <InfoBox
          isRed 
          active={casesType === "deaths"} 
          onClick={ (e) => setCasesTypes('deaths') }
          title="Deaths" cases={preetyPrintStat(countryInfo.todayDeaths)} total={preetyPrintStat(countryInfo.deaths)} />
        </div>

        {/* Map */}
        <Maps casesType={casesType} countries={mapCountries} center={mapCenter} zoom={mapZoom}/>
      </div>

      <Card className="app__right">
        <CardContent>
          {/* Countries table */}
          <h3>Live Cases by Country</h3>
          <Table countries={tableData} />
          {/* Line Graph */}
          <h3 className="app__graphTitle">Worldwide new {casesType}</h3>
          <LineGraph className="app__graph" casesType={casesType}/>
        </CardContent>
      </Card>
    </div>
  );
}

export default App;
